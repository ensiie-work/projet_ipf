CIBLE = codage
TEST = tests

all : $(CIBLE) $(TEST)

$(CIBLE) : projet.ml
	ocamlc -o $(CIBLE) projet.ml

$(TEST) : tests.ml
	ocamlc -o $(TEST) tests.ml

clean :
	rm -rf $(CIBLE) $(TEST) *.cmi *.cmo

re : clean all
