(* [split_string s] retourne la liste de caractères correspondant à la chaine [s] *)
let split_string s =
  let rec aux i n =
    if i >= n
    then []
    else (String.get s i)::aux (i+1) n
  in
    aux 0 (String.length s)
;;

(* [parse_input ()] lit deux lignes sur l'entrée standard du programme. 
   La première doit comporter un entier, la seconde une chaine de caractères. 
   Retourne un couple composé de l'entier de la première ligne et de la liste des caractères de la seconde.

   Lève l'exception [Failure "int_of_string"] si la première ligne ne représente pas un entier.
*)

let parse_input () =
  let nb_antennes = int_of_string (read_line ()) in
  let phrase = read_line () in
    nb_antennes,split_string phrase
;;

(* ========== PHASE 1 ========== *)

(* Information
   Dans la suite du code, un caractère 'valide' signifie un caractère ' ', ou entre le 'A' et le 'Z' inclus.
   Le sens 'avant' signifie le sens alphabétique, soit ' ' -> A -> ... -> Z.
   Le sens 'arrière' signifie le sens opposé, soit Z -> ... -> A -> ' '.
   Un 'couple_distance' valide est un couple de la forme (i,(a,b)) tel que a >= 0 et i >= 0.
*)

(* [dist_forw a b] calcule le nombre de lettres séparant les caractères a et b en sens avant.
   a et b doivent être donnés sous la forme de leur code ASCII.

   @requires a et b doivent représenter des caractères valides.
   @ensures retourne le nombre de lettres séparant a et b, b inclus, en sens avant.
*)
let rec dist_forw a b =
  if a=b then 0
  else if a=Char.code ' ' then 1+dist_forw (Char.code 'A') b
  else if a=Char.code 'Z' then 1+dist_forw (Char.code ' ') b
  else 1+dist_forw (a+1) b
;;

(* [move_forw n] renvoie une liste qui représente n déplacements de la roue en sens avant.

   @requires n >= 0.
   @ensures retourne une liste de n 'N'. Si n<0, retourne une liste vide.
*)
let rec move_forw n =
  if n<0 then []
  else if n=0 then []
  else if n=1 then ['N']
  else 'N'::move_forw (n-1)
;;

(* [move_back n] renvoie une liste qui représente n déplacements de la roue en sens arrière.

   @requires n >= 0.
   @ensures retourne une liste de n 'P'. Si n<0, retourne une liste vide.
*)
let rec move_back n =
  if n<0 then []
  else if n=0 then []
  else if n=1 then ['P']
  else 'P'::move_back (n-1)
;;

(* [split_list ll] transforme une liste de liste en une liste.
   Equivalent à List.flatten .
*)
let rec split_list ll =
  match ll with
    |(x::[])::tl -> x::split_list tl
    |(x::q)::tl -> x::split_list (q::tl)
    |[]::tl -> split_list tl
    |[] -> []
;;

(* [uppercase msg] transforme tous les caractères de la liste msg en Majuscule.

   @ensures retourne une liste de caractères valides
   @raise stoppe l'exécution du programme avec le message "mauvais caractere" si msg contient
   un caractère différent d'une lettre ou d'un espace
*)
let rec uppercase msg =
  match msg with
    | [] -> []
    |hd::tl -> if hd>='a' && hd <= 'z'
        then Char.chr (Char.code hd - 32) ::uppercase tl
        else if hd != ' ' && (hd < 'A' || hd > 'Z')
        then failwith "mauvais caractere"
        else hd::uppercase tl
;;

(*[commande l prec] retourne la liste correspondant aux déplacements de la roue (tourner, envoyer)
  que l'on doit effectuer pour coder le message l.

  @requires s doit être composée de caractères valides.
*)
let codage s =
  let rec commande l prec =
    match l with
      |[] -> []
      |hd::tl ->let curr = Char.code hd in
            if prec=curr then ['E']::commande tl curr
            else (
              let dist_f = dist_forw prec curr in
              let dist_b = 27 - dist_f in
                if dist_f <= dist_b then (move_forw dist_f)::commande l curr
                else (move_back dist_b)::commande l curr
            )
  in split_list (commande (uppercase s) (Char.code ' '))
;;

(* ========== PHASE 2 ========== *)

(* [dist a b num] retourne le minimum des distances entre a et b en comparant les sens alphabétique et opposé.
   a représente le caractère de la roue et b le caractère actuel du message.

   @requires num >= 0, a et b en code ASCII représentant des caractères valides.
   @ensures retourne un couple_distance (num,(dist,dir)) valide tel que :
   - num représente l'indice de la roue dont on a calculé la distance
   - dist représente la distance de la roue au caractère b
   - dir représente la direction : true si on a obtenu la distance en sens avant, false sinon
*)
let dist a b num =
  let dist_f = dist_forw a b in
  let dist_b = 27 - dist_f in
    if dist_f <= dist_b then (num,(dist_f,true))
    else (num,(dist_b,false))
;;

(* [cons_list_roue n i] construit la liste des roues, initialisées au caractère ' '.

   @requires n positif, i = 0.
   @ensures retourne une liste de n élément de type (i,' ') où i s'incrémente.
*)
let rec cons_list_roue n i =
  if n=0 then []
  else (i,Char.code ' ')::cons_list_roue (n-1) (i+1)
;;

(* [list_dist l b] construit une liste où le i-ème élément représente la distance de la
   i-ème roue au caractère b actuel du message.

   @requires l ne doit contenir que des éléments (i,c) avec i >= 0 et c un caractère valide.
   @ensures retourne une liste de couple_distance comme décrits dans 'dist a b'.
*)
let rec list_dist l b =
  match l with
    |[] -> []
    |(n,a)::tl -> (dist a b n)::list_dist tl b
;;

(* [compare_dist a b] fonction de comparaison entre deux couple_distance comme décrits dans 'dist a b'.

   @requires a et b des couple_distance valides.
   @ensures retourne le couple_distance qui a la distance la plus petite.
*)
let compare_dist a b =
  let (na,(da,dira)) = a in
  let (nb,(db,dirb)) = b in
    if da <= db then a else b
;;

(* [min_dist l] retourne le couple_distance avec la plus petite distance de la liste l.

   @requires l doit être une liste de couple_distance valides.
*)
let min_dist l = List.fold_left compare_dist (-1,(max_int,false)) l;;


(* [modif k v l] modifie la valeur de l'élément qui a pour clé k dans la liste de couples l.

   @requires (k,_) doit exister dans l (soit k >= 0).
   @ensures retourne la liste l où l'élément (k,x) devient l'élément (k,v).
*)
let rec modif k v l =
  match l with
    |[] -> []
    |(k',v')::tl -> if k'=k then (k',v)::tl
        else (k',v')::modif k v tl
;;

(* [list_diff_caracteres msg l] retourne une liste où chaque caractère de msg apparait une et une seule fois.

   @requires l = []
*)
let rec list_diff_caracteres msg l =
  match msg with
    | [] -> List.rev l
    | hd::tl -> if List.mem hd l then list_diff_caracteres tl l
        else list_diff_caracteres tl (hd::l)
;;

(* [fill_with_spaces l n] retourne une liste de couples représentant des roues en partant de l et en ajoutant
   n roues placées à espace après

   @requires n positif
   @requires i = 0
   @raises stoppe l'exécution avec une erreur si n < 0
*)
let rec fill_with_spaces l i n res =
  if n < 0 then failwith "mauvais appel à fill_with_spaces";
  match l with
    | [] -> (List.rev res)@cons_list_roue n i
    | hd::tl -> fill_with_spaces tl (i+1) n ((i,Char.code hd)::res)
;;

let print_list l = List.iter (Printf.printf "%c") l;;

(* [start_opti roues] affiche les instructions de codage des premières roues si l'on se trouve dans un cas où l'optimisation est possible
*)
let rec start_opti roues =
  match roues with
    | [] -> ()
    | (i,car)::tl -> 
        let d_f = dist_forw (Char.code ' ') car in
        let d_b = 27 - d_f in
          if i != 0 && d_f != 0 && d_b != 0 then Printf.printf "S%i" i;
          if d_f <= d_b then print_list (move_forw d_f)
          else print_list (move_back d_b);
          start_opti tl
;;

(* [codage_n_roues n msg] code le message msg en renvoyant la liste des actions à faire sur les roues.

   @requires n > 0, msg doit être une liste de caractères valides.
   @ensures retourne la liste des actions (tourner une roue, envoyer un message, changer de roue) pour coder
   le message msg.
*)
let codage_n_roues n msg =
  if n=1 then codage msg 
  else begin
    let liste_r = cons_list_roue n 0 in
    let list_carac = list_diff_caracteres (uppercase msg) [] in
    let rec aux l curr liste_roue =
      match l with
        |[] -> []
        |hd::tl -> let (n_min,(dis_min,dir)) = min_dist (list_dist liste_roue (Char.code hd)) in
            let liste_roue_next = (modif n_min (Char.code hd) liste_roue) in
              if curr != n_min then begin
                if dir
                then ('S'::split_string (string_of_int(n_min)))::move_forw dis_min::['E']::aux tl n_min liste_roue_next
                else ('S'::split_string (string_of_int(n_min)))::move_back dis_min::['E']::aux tl n_min liste_roue_next
              end
              else begin
                if dir then move_forw dis_min::['E']::aux tl n_min liste_roue_next
                else move_back dis_min::['E']::aux tl n_min liste_roue_next
              end
    in
      if List.length list_carac <= n && 2*List.length list_carac <= List.length msg then begin
        let roues = fill_with_spaces list_carac 0 (n-List.length list_carac) [] in
          start_opti roues;
          split_list (aux (uppercase msg) (List.length list_carac - 1) roues) end
      else split_list (aux (uppercase msg) 0 liste_r)
  end
;;

let (n,msg) = parse_input() in
let l = codage_n_roues n msg in
  List.iter (Printf.printf "%c") l;;

