# Projet_IPF

Projet de programmation fonctionnelle du S2 à l'ENSIIE

Lien du projet : http://web4.ensiie.fr/~forest/IPF/ipf_projet_2018_s2.html

## Création de l'exécutable

Les différentes commandes du Makefile sont :
- `make` ou `make codage` pour créer l'exécutable *codage*
- `make tests` pour créer un exécutable *tests* qui lance des tests (dans le fichier tests.ml) sur des fonctions du programme
- `make clean` pour supprimer tous les fichiers créés par le Makefile
- `make re` pour supprimer puis re-créer les exécutables
 
## Utilisation de l'exécutable

l'exécutable *codage* se lance sans argument. Une fois lancé, le terminal attend
des entrées utilisateurs : une pour le nombre de roues, une pour le message à
coder. le message doit être en majuscule, sans guillemets. Par exemple, pour
lancer le codage avec 3 roues sur le message "BONJOUR", on lance :
```
./codage
3
BONJOUR
```
