let split_string s =
  let rec aux i n =
    if i >= n
    then []
    else (String.get s i)::aux (i+1) n
  in
    aux 0 (String.length s)
;;

let parse_input () =
  let nb_antennes = int_of_string (read_line ()) in
  let phrase = read_line () in
    nb_antennes,split_string phrase
;;

(* ====== PHASE 1 ========== *)

let step() = Printf.printf "===================================\n";;

let rec print_list l =
  List.iter (Printf.printf "%c") l
;;

Printf.printf "\nTests des différentes fonctions\n\n"; step();;

let rec dist_forw a b =
  if a=b then 0
  else if a=Char.code ' ' then 1+dist_forw (Char.code 'A') b
  else if a=Char.code 'Z' then 1+dist_forw (Char.code ' ') b
  else 1+dist_forw (a+1) b
;;

Printf.printf "Test: dist_forw A C; Expected: 2\n";;
Printf.printf " Result: %i\n" (dist_forw (Char.code 'A') (Char.code 'C'));;
step();;

let rec move_forw n =
  if n<0 then []
  else if n=0 then []
  else if n=1 then ['N']
  else 'N'::move_forw (n-1)
;;

Printf.printf "Test: move_forw 3; Expected: NNN\n";;
Printf.printf " Result: ";print_list (move_forw 3); Printf.printf "\n";;
step();;

let rec move_back n =
  if n<0 then []
  else if n=0 then []
  else if n=1 then ['P']
  else 'P'::move_back (n-1)
;;

Printf.printf "Test: move_back 2; Expected: PP\n";;
Printf.printf " Result: ";print_list (move_back 2);Printf.printf "\n";;
step();;

let rec split_list ll =
  match ll with
    |(x::[])::tl -> x::split_list tl
    |(x::q)::tl -> x::split_list (q::tl)
    |[]::tl -> split_list tl
    |[] -> []
;;

Printf.printf "Test: split_list [['A';'B'];['C']]; Expected: ABC\n";;
Printf.printf " Result: ";print_list (split_list [['A';'B'];['C']]);Printf.printf "\n";;
step();;

let rec uppercase msg =
  match msg with
    | [] -> []
    |hd::tl -> if hd>='a' && hd <= 'z'
        then Char.chr (Char.code hd - 32) ::uppercase tl
        else if hd != ' ' && (hd < 'A' || hd > 'Z')
        then failwith "mauvais caractere"
        else hd::uppercase tl
;;

Printf.printf "Test: uppercase maJusCuLE; Expected: MAJUSCULE\n";;
Printf.printf " Result: "; print_list (uppercase (split_string "maJusCuLE"));Printf.printf "\n";;
step();;

let codage s =
  let rec commande l prec =
    match l with
      |[] -> []
      |hd::tl ->let curr = Char.code hd in
            if prec=curr then ['E']::commande tl curr
            else (
              let dist_f = dist_forw prec curr in
              let dist_b = 27 - dist_f in
                if dist_f <= dist_b then (move_forw dist_f)::commande l curr
                else (move_back dist_b)::commande l curr
            )
  in split_list (commande (uppercase s) (Char.code ' '))
;;

Printf.printf "Test: codage ab z; Expected: NENEPPEPE\n";;
Printf.printf " Result: ";print_list (codage (split_string "ab z"));Printf.printf"\n";;
step();;

(* ========== PHASE 2 ========== *)

let dist a b num =
  let dist_f = dist_forw a b in
  let dist_b = 27 - dist_f in
    if dist_f <= dist_b then (num,(dist_f,true))
    else (num,(dist_b,false))
;;

Printf.printf "Test: dist Z A 3; Expected: (3,(2,true))\n";;
let (num,(s,bol)) = dist (Char.code 'Z') (Char.code 'A') 3 in
Printf.printf " Result: (%i,(%i," num s;
	if bol then Printf.printf "true)\n" else Printf.printf "false)\n";;
step();;


let rec cons_list_roue n i =
  if n=0 then []
  else (i,Char.code ' ')::cons_list_roue (n-1) (i+1)
;;

let rec list_dist l b =
  match l with
    |[] -> []
    |(n,a)::tl -> (dist a b n)::list_dist tl b
;;

let compare_dist a b =
  let (na,(da,dira)) = a in
  let (nb,(db,dirb)) = b in
    if da <= db then a else b
;;

let min_dist l = List.fold_left compare_dist (-1,(max_int,false)) l;;

let rec modif k v l =
  match l with
    |[] -> []
    |(k',v')::tl -> if k'=k then (k',v)::tl
        else (k',v')::modif k v tl
;;

Printf.printf "Test: modif 2 3 [(1,12);(2,25)]; Expected: [(1,12);(2,3);]\n";;
if modif 2 3 [(1,12);(2,25)] = [(1,12);(2,3)] then Printf.printf " Result: OK\n";;
step();;

let rec list_diff_caracteres msg l =
  match msg with
    | [] -> List.rev l
    | hd::tl -> if List.mem hd l then list_diff_caracteres tl l
        else list_diff_caracteres tl (hd::l)
;;

let rec fill_with_spaces l i n res =
  if n < 0 then failwith "mauvais appel à fill_with_spaces";
  match l with
    | [] -> (List.rev res)@cons_list_roue n i
    | hd::tl -> fill_with_spaces tl (i+1) n ((i,Char.code hd)::res)
;;

let print_list l = List.iter (Printf.printf "%c") l;;

let rec start_opti roues =
  match roues with
    | [] -> ()
    | (i,car)::tl -> 
        let d_f = dist_forw (Char.code ' ') car in
        let d_b = 27 - d_f in
          if i != 0 && d_f != 0 && d_b != 0 then Printf.printf "S%i" i;
          if d_f <= d_b then print_list (move_forw d_f)
          else print_list (move_back d_b);
          start_opti tl
;;


let codage_n_roues n msg =
  if n=1 then codage msg 
  else begin
    let liste_r = cons_list_roue n 0 in
    let list_carac = list_diff_caracteres (uppercase msg) [] in
    let rec aux l curr liste_roue =
      match l with
        |[] -> []
        |hd::tl -> let (n_min,(dis_min,dir)) = min_dist (list_dist liste_roue (Char.code hd)) in
            let liste_roue_next = (modif n_min (Char.code hd) liste_roue) in
              if curr != n_min then begin
                if dir
                then ('S'::split_string (string_of_int(n_min)))::move_forw dis_min::['E']::aux tl n_min liste_roue_next
                else ('S'::split_string (string_of_int(n_min)))::move_back dis_min::['E']::aux tl n_min liste_roue_next
              end
              else begin
                if dir then move_forw dis_min::['E']::aux tl n_min liste_roue_next
                else move_back dis_min::['E']::aux tl n_min liste_roue_next
              end
    in
      if List.length list_carac <= n && 2*List.length list_carac <= List.length msg then begin
        let roues = fill_with_spaces list_carac 0 (n-List.length list_carac) [] in
          start_opti roues;
          split_list (aux (uppercase msg) (List.length list_carac - 1) roues) end
      else split_list (aux (uppercase msg) 0 liste_r)
  end
;;

Printf.printf "Test: codage_n_roues 2 AZ; Expected: NES1PE\n";;
Printf.printf " Result: "; print_list(codage_n_roues 2 ['A';'Z']);Printf.printf "\n";;
step();;

Printf.printf "Test: codage_n_roues 3 ABCABCABCABC; Expected: NS1NNS2NNNS0ES1ES2ES0ES1ES2ES0ES1ES2ES0ES1ES2E\n";;
Printf.printf " Result: "; print_list(codage_n_roues 3 (split_string "abcabcabcabc"));Printf.printf "\n";;
step();;


